package com.fercugliandro.mastertech.contato.repository;

import com.fercugliandro.mastertech.contato.model.Contato;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContatoRepository extends JpaRepository<Contato, Long> {

    @Query("SELECT c FROM Contato c WHERE idPrincipal = ?1 ")
    Optional<List<Contato>> findContatosByIdPrincipal(long idPrincipal);

}
