package com.fercugliandro.mastertech.contato.model;

import com.fercugliandro.mastertech.contato.model.dto.request.CreateContatoRequest;
import org.springframework.stereotype.Component;

@Component
public class ContatoMapper {

    public Contato toContato(CreateContatoRequest request) {
        Contato contato = new Contato();
        contato.setNome(request.getNome());
        contato.setTelefone(request.getTelefone());

        return contato;
    }

}
