package com.fercugliandro.mastertech.contato.model;

import javax.persistence.*;

@Entity
public class Contato {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "idprincipal")
    private long idPrincipal;

    @Column
    private String nome;

    @Column
    private String telefone;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdPrincipal() {
        return idPrincipal;
    }

    public void setIdPrincipal(long idPrincipal) {
        this.idPrincipal = idPrincipal;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
