package com.fercugliandro.mastertech.contato.service.impl;

import com.fercugliandro.mastertech.contato.model.Contato;
import com.fercugliandro.mastertech.contato.repository.ContatoRepository;
import com.fercugliandro.mastertech.contato.service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ContatoServiceImpl implements ContatoService {

    @Autowired
    private ContatoRepository repository;

    @Override
    public Contato criarContato(Contato contato) {
        return repository.save(contato);
    }

    @Override
    public List<Contato> obterTodosContatos(long idPrincipal) {

        Optional<List<Contato>> contatos = repository.findContatosByIdPrincipal(idPrincipal);
        if (!contatos.isPresent())
            return null;

        return contatos.get();
    }
}
