package com.fercugliandro.mastertech.contato.service;

import com.fercugliandro.mastertech.contato.model.Contato;
import org.springframework.stereotype.Service;

import java.util.List;

public interface ContatoService {

    Contato criarContato(Contato contato);

    List<Contato> obterTodosContatos(long idPrincipal);

}
