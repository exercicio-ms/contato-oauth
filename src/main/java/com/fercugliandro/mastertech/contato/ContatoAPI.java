package com.fercugliandro.mastertech.contato;

import com.fercugliandro.mastertech.contato.model.Contato;
import com.fercugliandro.mastertech.contato.model.ContatoMapper;
import com.fercugliandro.mastertech.contato.model.Usuario;
import com.fercugliandro.mastertech.contato.model.dto.request.CreateContatoRequest;
import com.fercugliandro.mastertech.contato.service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ContatoAPI {

    @Autowired
    private ContatoService service;

    @Autowired
    private ContatoMapper mapper;

    @GetMapping("/whoami")
    public String whoami(@AuthenticationPrincipal(expression="name") String name) {
        return name;
    }

    @GetMapping("/contatos")
    public ResponseEntity<?> obterContatos(@AuthenticationPrincipal Usuario usuario) {
        List<Contato> contatos = service.obterTodosContatos(usuario.getId());
        return new ResponseEntity<>(contatos, HttpStatus.OK);
    }

    @PostMapping("/contato")
    public ResponseEntity<?> criarContato(@AuthenticationPrincipal Usuario usuario, @RequestBody CreateContatoRequest request) {

        try {

            Contato contato = mapper.toContato(request);
            contato.setIdPrincipal(usuario.getId());

            Contato contatoResponse = service.criarContato(contato);
            return new ResponseEntity<>(contatoResponse, HttpStatus.CREATED);
        } catch (Exception e) {
            String errorMessage;
            errorMessage = e.getMessage() + " <== error";
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }

    }
}
